/*
 * Copyright © 2008-2018 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open
 * Source Software License located at [https://www.pinnacle21.com/license]
 * (the "License").
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, and is distributed "AS IS," "WITH ALL FAULTS," and
 * without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.
 * See the License for more details.
 */

package net.pinnacle21.validation.shim.models;

import net.pinnacle21.validator.api.model.DataDetails;

public class DataDetailsShim implements DataDetails {
    private final org.opencdisc.validator.model.DataDetails details;

    public DataDetailsShim(org.opencdisc.validator.model.DataDetails details) {
        this.details = details;
    }

    @Override
    public int getId() {
        return this.details.getID();
    }

    @Override
    public Info getInfo() {
        return Info.valueOf(this.details.getInfo().name());
    }

    @Override
    public String getKey() {
        return this.details.getKey();
    }

    @Override
    public String getName() {
        return this.details.getName();
    }
}
