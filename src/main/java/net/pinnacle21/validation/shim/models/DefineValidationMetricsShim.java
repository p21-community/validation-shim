/*
 * Copyright © 2008-2018 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open
 * Source Software License located at [https://www.pinnacle21.com/license]
 * (the "License").
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, and is distributed "AS IS," "WITH ALL FAULTS," and
 * without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.
 * See the License for more details.
 */

package net.pinnacle21.validation.shim.models;

import net.pinnacle21.validator.api.model.Diagnostic;
import net.pinnacle21.validator.api.model.RuleMetrics;
import net.pinnacle21.validator.api.model.SourceDetails;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class DefineValidationMetricsShim implements RuleMetrics {
    private static final String DEFINE_DOMAIN = "DEFINE";

    private final Map<String, Map<String, RuleMetric>> rules = new HashMap<>();

    public void recordFailure(Diagnostic diagnostic) {
        RuleMetric rule = this.getRule(diagnostic.getId(), diagnostic.getContext(), true, diagnostic.getMessage(),
                diagnostic.getType());

        if (rule != null) {
            rule.increment();
        }
    }

    @Override
    public Set<String> getDomains() {
        return Collections.singleton(DEFINE_DOMAIN);
    }

    @Override
    public Set<String> getDomainRules(String domain) {
        if (isNotDefine(domain)) {
            return Collections.emptySet();
        }

        return this.rules.keySet();
    }

    @Override
    public Map<String, Set<String>> getDomainContexts(String domain) {
        if (isNotDefine(domain)) {
            return Collections.emptyMap();
        }

        Map<String, Set<String>> contexts = new HashMap<>();

        for (Map.Entry<String, Map<String, RuleMetric>> rule : this.rules.entrySet()) {
            contexts.put(rule.getKey(), Collections.unmodifiableSet(rule.getValue().keySet()));
        }

        return contexts;
    }

    @Override
    public long getExecutionCount() {
        return this.getDomainExecutionCount(DEFINE_DOMAIN);
    }

    @Override
    public long getFailureCount() {
        return this.getDomainFailureCount(DEFINE_DOMAIN);
    }

    @Override
    public long getInvocationCount() {
        return this.getDomainInvocationCount(DEFINE_DOMAIN);
    }

    @Override
    public long getDomainExecutionCount(String domain) {
        return this.getDomainFailureCount(domain, null);
    }

    @Override
    public long getDomainExecutionCount(String domain, Diagnostic.Type type) {
        return this.getDomainFailureCount(domain, type);
    }

    @Override
    public long getDomainFailureCount(String domain) {
        return this.getDomainFailureCount(domain, null);
    }

    @Override
    public long getDomainFailureCount(String domain, Diagnostic.Type type) {
        if (isNotDefine(domain)) {
            return 0;
        }

        return this.rules.values()
                .stream()
                .flatMap(m -> m.values().stream())
                .filter(r -> type == null || r.getType() == type)
                .mapToLong(RuleMetric::getFailures)
                .sum();
    }

    @Override
    public long getDomainInvocationCount(String domain) {
        return this.getDomainFailureCount(domain, null);
    }

    @Override
    public long getDomainInvocationCount(String domain, Diagnostic.Type type) {
        return this.getDomainFailureCount(domain, type);
    }

    @Override
    public int getRuleExecutionCount(String domain, String id, String context) {
        return this.getRuleFailureCount(domain, id, context);
    }

    @Override
    public int getRuleFailureCount(String domain, String id, String context) {
        if (isNotDefine(domain)) {
            return 0;
        }

        RuleMetric rule = this.getRule(id, context, false, null, null);

        return rule != null
                ? rule.getFailures()
                : 0;
    }

    @Override
    public int getRuleInvocationCount(String domain, String id, String context) {
        return this.getRuleFailureCount(domain, id, context);
    }

    @Override
    public long getRuleElapsedCount(String domain, String id, String context) {
        if (isNotDefine(domain)) {
            return 0;
        }

        return 1;
    }

    @Override
    public SourceDetails.Reference getRuleReference(String domain, String id, String context) {
        if (isNotDefine(domain)) {
            return null;
        }

        return SourceDetails.Reference.Data;
    }

    @Override
    public String getRuleMessage(String domain, String id, String context) {
        if (isNotDefine(domain)) {
            return null;
        }

        RuleMetric rule = this.getRule(id, context, false, null, null);

        return rule != null
                ? rule.getMessage()
                : null;
    }

    @Override
    public Diagnostic.Type getRuleType(String domain, String id, String context) {
        if (isNotDefine(domain)) {
            return null;
        }

        RuleMetric rule = this.getRule(id, context, false, null, null);

        return rule != null
                ? rule.getType()
                : null;
    }

    private RuleMetric getRule(String id, String context, boolean create, String message, Diagnostic.Type type) {
        Map<String, RuleMetric> instances = this.rules.get(id);

        if (instances == null) {
            if (create) {
                this.rules.put(id, instances = new HashMap<>());
            } else {
                return null;
            }
        }

        RuleMetric rule = instances.get(context);

        if (rule == null) {
            if (create) {
                instances.put(context, rule = new RuleMetric(message, type));
            } else {
                return null;
            }
        }

        return rule;
    }

    private static boolean isNotDefine(String name) {
        return !DEFINE_DOMAIN.equalsIgnoreCase(name);
    }

    private static class RuleMetric {
        private final String message;
        private final Diagnostic.Type type;
        private int failures = 0;

        private RuleMetric(String message, Diagnostic.Type type) {
            this.message = message;
            this.type = type;
        }

        void increment() {
            this.failures++;
        }

        Diagnostic.Type getType() {
            return this.type;
        }

        String getMessage() {
            return this.message;
        }

        int getFailures() {
            return this.failures;
        }
    }
}
