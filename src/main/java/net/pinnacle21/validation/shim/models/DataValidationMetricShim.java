/*
 * Copyright © 2008-2018 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open
 * Source Software License located at [https://www.pinnacle21.com/license]
 * (the "License").
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, and is distributed "AS IS," "WITH ALL FAULTS," and
 * without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.
 * See the License for more details.
 */

package net.pinnacle21.validation.shim.models;

import com.google.common.collect.HashBasedTable;
import com.google.common.collect.Table;
import net.pinnacle21.validator.api.model.Diagnostic;
import net.pinnacle21.validator.api.model.RuleInstance;
import net.pinnacle21.validator.api.model.RuleMetrics;
import net.pinnacle21.validator.api.model.SourceDetails;
import org.apache.commons.lang3.tuple.Pair;
import org.opencdisc.validator.model.EntityDetails;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.stream.Stream;

public class DataValidationMetricShim implements RuleMetrics {
    private final Table<String, String, Map<String, Diagnostic.Type>> ruleTypes = HashBasedTable.create();
    private org.opencdisc.validator.model.RuleMetrics internalMetrics;

    public void setInternalMetrics(org.opencdisc.validator.model.RuleMetrics metrics) {
        this.internalMetrics = metrics;
    }

    public RuleInstance record(RuleInstance rule) {
        if (!this.ruleTypes.contains(rule.getDomain(), rule.getId())) {
            this.ruleTypes.put(rule.getDomain(), rule.getId(), new HashMap<>());
        }

        this.ruleTypes.get(rule.getDomain(), rule.getId()).put(rule.getContext(), rule.getType());

        return rule;
    }

    @Override
    public Set<String> getDomains() {
        return this.ruleTypes.rowKeySet();
    }

    @Override
    public Set<String> getDomainRules(String domain) {
        return this.ruleTypes.row(domain).keySet();
    }

    @Override
    public Map<String, Set<String>> getDomainContexts(String domain) {
        return this.internalMetrics.getDomainContexts(domain);
    }

    @Override
    public long getExecutionCount() {
        return this.internalMetrics.getDomains()
                .stream()
                .mapToLong(this::getDomainExecutionCount)
                .sum();
    }

    @Override
    public long getFailureCount() {
        return this.internalMetrics.getDomains()
                .stream()
                .mapToLong(this::getDomainFailureCount)
                .sum();
    }

    @Override
    public long getInvocationCount() {
        return this.internalMetrics.getDomains()
                .stream()
                .mapToLong(this::getDomainInvocationCount)
                .sum();
    }

    @Override
    public long getDomainExecutionCount(String domain) {
        return this.getDomainExecutionCount(domain, null);
    }

    @Override
    public long getDomainExecutionCount(String domain, Diagnostic.Type type) {
        return this.getFlattenedRules(domain, type)
                .mapToLong(r -> this.getRuleExecutionCount(domain, r.getKey(), r.getValue()))
                .sum();
    }

    @Override
    public long getDomainFailureCount(String domain) {
        return this.getDomainFailureCount(domain, null);
    }

    @Override
    public long getDomainFailureCount(String domain, Diagnostic.Type type) {
        return this.getFlattenedRules(domain, type)
                .mapToLong(r -> this.getRuleFailureCount(domain, r.getKey(), r.getValue()))
                .sum();
    }

    @Override
    public long getDomainInvocationCount(String domain) {
        return this.getDomainInvocationCount(domain, null);
    }

    @Override
    public long getDomainInvocationCount(String domain, Diagnostic.Type type) {
        return this.getFlattenedRules(domain, type)
                .mapToLong(r -> this.getRuleInvocationCount(domain, r.getKey(), r.getValue()))
                .sum();
    }

    @Override
    public int getRuleExecutionCount(String domain, String id, String context) {
        return this.internalMetrics.getDomainExecutionCount(domain, id, context);
    }

    @Override
    public int getRuleFailureCount(String domain, String id, String context) {
        return this.internalMetrics.getDomainFailureCount(domain, id, context);
    }

    @Override
    public int getRuleInvocationCount(String domain, String id, String context) {
        return this.internalMetrics.getDomainInvocationCount(domain, id, context);
    }

    @Override
    public long getRuleElapsedCount(String domain, String id, String context) {
        return this.internalMetrics.getDomainElapsedCount(domain, id, context);
    }

    @Override
    public SourceDetails.Reference getRuleReference(String domain, String id, String context) {
        EntityDetails.Reference reference = this.internalMetrics.getReference(domain, id, context);

        return reference != null
                ? SourceDetails.Reference.valueOf(reference.name())
                : null;
    }

    @Override
    public String getRuleMessage(String domain, String id, String context) {
        return this.internalMetrics.getMessage(domain, id, context);
    }

    @Override
    public Diagnostic.Type getRuleType(String domain, String id, String context) {
        Map<String, Diagnostic.Type> types = this.ruleTypes.get(domain, id);

        return types != null
                ? types.get(context)
                : null;
    }

    private Stream<Pair<String, String>> getFlattenedRules(String domain, Diagnostic.Type typeFilter) {
        return this.internalMetrics.getDomainContexts(domain)
                .entrySet()
                .stream()
                .flatMap(e -> e.getValue()
                        .stream()
                        .map(c -> Pair.of(e.getKey(), c))
                )
                .filter(p -> {
                    if (typeFilter == null) {
                        return true;
                    }

                    Map<String, Diagnostic.Type> types = this.ruleTypes.get(domain, p.getKey());

                    return types != null && types.get(p.getValue()) == typeFilter;
                });
    }
}
