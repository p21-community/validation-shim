/*
 * Copyright © 2008-2018 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open
 * Source Software License located at [https://www.pinnacle21.com/license]
 * (the "License").
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, and is distributed "AS IS," "WITH ALL FAULTS," and
 * without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.
 * See the License for more details.
 */

package net.pinnacle21.validation.shim.models;

import net.pinnacle21.validator.api.model.VariableDetails;

public class VariableDetailsShim implements VariableDetails {
    private final org.opencdisc.validator.model.VariableDetails details;

    VariableDetailsShim(org.opencdisc.validator.model.VariableDetails details) {
        this.details = details;
    }

    @Override
    public boolean getBoolean(Property property) {
        return this.details.getBoolean(convert(property));
    }

    @Override
    public Boolean getBoolean(Property property, Boolean defaultValue) {
        return this.details.hasProperty(convert(property))
                ? this.details.getBoolean(convert(property))
                : defaultValue;
    }

    @Override
    public int getInteger(Property property) {
        return this.details.getInteger(convert(property));
    }

    @Override
    public Integer getInteger(Property property, Integer defaultValue) {
        return this.details.hasProperty(convert(property))
                ? this.details.getInteger(convert(property))
                : defaultValue;
    }

    @Override
    public String getString(Property property) {
        return this.details.getString(convert(property));
    }

    @Override
    public String getString(Property property, String defaultValue) {
        return this.details.hasProperty(convert(property))
                ? this.details.getString(convert(property))
                : defaultValue;
    }

    @Override
    public boolean hasProperty(Property property) {
        return this.details.hasProperty(convert(property));
    }

    private static org.opencdisc.validator.model.VariableDetails.Property convert(VariableDetails.Property property) {
        return org.opencdisc.validator.model.VariableDetails.Property.valueOf(property.name());
    }
}
