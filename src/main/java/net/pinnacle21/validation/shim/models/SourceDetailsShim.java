/*
 * Copyright © 2008-2018 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open
 * Source Software License located at [https://www.pinnacle21.com/license]
 * (the "License").
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, and is distributed "AS IS," "WITH ALL FAULTS," and
 * without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.
 * See the License for more details.
 */

package net.pinnacle21.validation.shim.models;

import net.pinnacle21.validator.api.model.SourceDetails;
import net.pinnacle21.validator.api.model.VariableDetails;
import org.opencdisc.validator.model.EntityDetails;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class SourceDetailsShim implements SourceDetails {
    private final EntityDetails details;

    public SourceDetailsShim(EntityDetails details) {
        this.details = details;
    }

    @Override
    public boolean getBoolean(Property property) {
        return this.details.getBoolean(convert(property));
    }

    @Override
    public Boolean getBoolean(Property property, Boolean defaultValue) {
        return this.details.hasProperty(convert(property))
                ? this.details.getBoolean(convert(property))
                : defaultValue;
    }

    @Override
    public int getInteger(Property property) {
        return this.details.getInteger(convert(property));
    }

    @Override
    public Integer getInteger(Property property, Integer defaultValue) {
        return this.details.hasProperty(convert(property))
                ? (Integer)this.details.getInteger(convert(property))
                : defaultValue;
    }

    @Override
    public SourceDetails getParent() {
        EntityDetails parent = this.details.getParent();

        return parent != null
                ? new SourceDetailsShim(parent)
                : null;
    }

    @Override
    public Reference getReference() {
        return Reference.valueOf(this.details.getReference().name());
    }

    @Override
    public String getString(Property property) {
        return this.getStringInternal(property);
    }

    @Override
    public String getString(Property property, String defaultValue) {
        return this.details.hasProperty(convert(property))
                ? this.getStringInternal(property)
                : defaultValue;
    }

    @Override
    public boolean hasVariable(String name) {
        return this.details.hasVariable(name);
    }

    @Override
    public VariableDetails getVariable(String name) {
        return Optional.ofNullable(this.details.getVariable(name))
                .map(VariableDetailsShim::new)
                .orElse(null);
    }

    @Override
    public List<VariableDetails> getVariables() {
        return this.details.getVariables()
                .stream()
                .map(VariableDetailsShim::new)
                .collect(Collectors.toList());
    }

    @Override
    public boolean hasProperty(Property property) {
        return this.details.hasProperty(convert(property));
    }

    @Override
    public List<SourceDetails> getSplitSources() {
        return this.details.getSubentities()
                .stream()
                .map(SourceDetailsShim::new)
                .collect(Collectors.toList());
    }

    private String getStringInternal(Property property) {
        String value = this.details.getString(convert(property));

        // We want to uppercase here for consistency...
        if (property == Property.Name && value.equalsIgnoreCase("define")) {
            value = "DEFINE";
        }

        return value;
    }

    private static EntityDetails.Property convert(SourceDetails.Property property) {
        return EntityDetails.Property.valueOf(property.name());
    }
}
