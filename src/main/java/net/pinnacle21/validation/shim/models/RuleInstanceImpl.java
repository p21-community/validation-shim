/*
 * Copyright © 2008-2018 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open
 * Source Software License located at [https://www.pinnacle21.com/license]
 * (the "License").
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, and is distributed "AS IS," "WITH ALL FAULTS," and
 * without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.
 * See the License for more details.
 */

package net.pinnacle21.validation.shim.models;

import net.pinnacle21.validator.api.model.Diagnostic;
import net.pinnacle21.validator.api.model.RuleInstance;
import org.opencdisc.validator.model.Message;
import org.opencdisc.validator.report.MessageTemplate;

public class RuleInstanceImpl implements RuleInstance {
    private final String domain;
    private final String id;
    private final String context;
    private final String category;
    private final String message;
    private final String description;
    private final Diagnostic.Type type;

    public RuleInstanceImpl(MessageTemplate template) {
        this(null, template.getID(), null, template.getCategory(), template.getMessage(), template.getDescription(),
                template.getType());
    }

    public RuleInstanceImpl(String domain, String id, String context, String category, String message, String description,
            Message.Type type) {
        this.domain = domain;
        this.id = id;
        this.context = context;
        this.category = category;
        this.message = message;
        this.description = description;
        this.type = convert(type);
    }

    @Override
    public String getDomain() {
        return this.domain;
    }

    @Override
    public String getContext() {
        return this.context;
    }

    @Override
    public String getCategory() {
        return this.category;
    }

    @Override
    public String getId() {
        return this.id;
    }

    @Override
    public String getDescription() {
        return this.description;
    }

    @Override
    public String getMessage() {
        return this.message;
    }

    @Override
    public String getPublisherId() {
        return null;
    }

    @Override
    public Diagnostic.Type getType() {
        return this.type;
    }

    private static Diagnostic.Type convert(Message.Type type) {
        return Diagnostic.Type.valueOf(type.name());
    }
}
