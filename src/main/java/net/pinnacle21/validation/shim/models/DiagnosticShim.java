/*
 * Copyright © 2008-2018 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open
 * Source Software License located at [https://www.pinnacle21.com/license]
 * (the "License").
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, and is distributed "AS IS," "WITH ALL FAULTS," and
 * without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.
 * See the License for more details.
 */

package net.pinnacle21.validation.shim.models;

import net.pinnacle21.validation.shim.util.Hex;
import net.pinnacle21.validator.api.model.DataDetails;
import net.pinnacle21.validator.api.model.Diagnostic;
import net.pinnacle21.validator.api.model.SourceDetails;
import org.opencdisc.validator.model.Message;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.*;

public class DiagnosticShim implements Diagnostic {
    private final Message message;
    private final SourceDetails sourceDetails;
    private final DataDetails dataDetails;
    private final String context;

    public DiagnosticShim(Message message) {
        this.message = message;
        this.sourceDetails = new SourceDetailsShim(message.getEntityDetails());
        this.dataDetails = message.getRecordDetails() != null
                ? new DataDetailsShim(message.getRecordDetails())
                : null;
        this.context = this.sourceDetails.getString(SourceDetails.Property.Name).equalsIgnoreCase("DEFINE")
                ? Hex.sha512(message.getMessage()).substring(0, 15)
                : null;
    }

    @Override
    public String getId() {
        return this.message.getID();
    }

    @Override
    public String getMessage() {
        return this.message.getMessage();
    }

    @Override
    public String getDescription() {
        return this.message.getDescription();
    }

    @Override
    public String getCategory() {
        return this.message.getCategory();
    }

    @Override
    public Type getType() {
        return Diagnostic.Type.valueOf(this.message.getType().name());
    }

    @Override
    public String getContext() {
        return this.context != null
                ? this.context
                : this.message.getContext();
    }

    @Override
    public long getTimestamp() {
        return LocalDateTime.parse(this.message.getPostTimestamp())
                .atOffset(ZoneOffset.UTC)
                .toInstant()
                .toEpochMilli();
    }

    @Override
    public SourceDetails getSourceDetails() {
        return this.sourceDetails;
    }

    @Override
    public DataDetails getDataDetails() {
        return this.dataDetails;
    }

    @Override
    public Set<String> getVariables() {
        return this.message.getVariables();
    }

    @Override
    public String getVariable(String variable) {
        return this.message.getValueOf(variable);
    }

    @Override
    public Map<String, String> getVariableValues() {
        Map<String, String> values = new LinkedHashMap<>();

        for (String value : this.message.getVariables()) {
            values.put(value, this.message.getValueOf(value));
        }

        return values;
    }

    @Override
    public Set<String> getProperties() {
        return Collections.emptySet();
    }

    @Override
    public String getProperty(String property) {
        return null;
    }

    @Override
    public Map<String, String> getPropertyValues() {
        return Collections.emptyMap();
    }
}
