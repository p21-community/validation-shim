/*
 * Copyright © 2008-2018 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open
 * Source Software License located at [https://www.pinnacle21.com/license]
 * (the "License").
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, and is distributed "AS IS," "WITH ALL FAULTS," and
 * without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.
 * See the License for more details.
 */

package net.pinnacle21.validation.shim;

import net.pinnacle21.validation.shim.models.DataValidationMetricShim;
import net.pinnacle21.validation.shim.models.DiagnosticShim;
import net.pinnacle21.validation.shim.models.RuleInstanceImpl;
import net.pinnacle21.validation.shim.models.SourceDetailsShim;
import net.pinnacle21.validation.shim.util.ShimValidator;
import net.pinnacle21.validator.api.BaseValidation;
import net.pinnacle21.validator.api.events.ValidationEvent;
import net.pinnacle21.validator.api.events.util.Dispatcher;
import net.pinnacle21.validator.api.model.*;
import net.pinnacle21.validator.api.model.ConfigOptions;
import net.pinnacle21.validator.api.model.SourceOptions;
import org.apache.commons.lang3.tuple.Pair;
import org.opencdisc.validator.*;
import org.opencdisc.validator.engine.ValidationEngine;
import org.opencdisc.validator.model.EntityDetails;
import org.opencdisc.validator.model.Message;
import org.opencdisc.validator.report.MessageTemplate;
import org.opencdisc.validator.rules.AbstractValidationRule;
import org.opencdisc.validator.rules.ValidationRule;
import org.opencdisc.validator.settings.Configuration;
import org.opencdisc.validator.util.Settings;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.Field;
import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;
import java.util.function.Consumer;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class DataValidationShim extends BaseValidation<DataValidationShim> {
    private static final Logger LOGGER = LoggerFactory.getLogger(DataValidationShim.class);
    private static final Dispatcher DISPATCHER = new Dispatcher(c -> LoggerFactory.getLogger(c)::error);

    private final List<SourceOptions> sources;
    private final ConfigOptions config;
    private final ValidationOptions options;

    DataValidationShim(List<SourceOptions> sources, ConfigOptions config, ValidationOptions options) {
        this.sources = sources;
        this.config = config;
        this.options = options != null
                ? options
                : ValidationOptions.builder().build();
    }

    @Override
    protected DataValidationShim recreate() {
        return new DataValidationShim(this.sources, this.config, this.options);
    }

    @Override
    public CompletionStage<ValidationResult> executeAsync(CancellationToken cancellationToken) {
        DataValidationMetricShim metrics = new DataValidationMetricShim();
        ValidationResult.Builder resultBuilder = ValidationResult.builder()
                .withMetrics(metrics);

        Set<SourceDetails> sourceDetails = Collections.synchronizedSet(new LinkedHashSet<>());

        return CompletableFuture.supplyAsync(() -> {
            // We do classloader shenanigans too, hooray! -_-
            ClassLoader original = Thread.currentThread().getContextClassLoader();

            Thread.currentThread().setContextClassLoader(DataValidationShim.class.getClassLoader());

            try {
                LOGGER.debug("Starting new validation via shim");

                Settings.set("Engine.TabularImplementation", "ShimValidator");
                this.options.getProperties().forEach(p -> Settings.set(p, this.options.getProperty(p)));

                List<org.opencdisc.validator.SourceOptions> sources = this.sources.stream()
                        .map(DataValidationShim::convert)
                        .collect(Collectors.toList());

                LOGGER.trace("Prepared {} sources for validation", sources.size());

                Validator validator = ValidatorFactory.newInstance().newValidator();

                try {
                    LOGGER.trace("Forcing our shim validator implementation in via reflection");

                    synchronized (DataValidationShim.class) {
                        Field engines = validator.getClass().getDeclaredField("ENGINES");

                        engines.setAccessible(true);
                        //noinspection unchecked
                        ((Map<String, Class<? extends ValidationEngine>>)engines.get(null))
                                .put("ShimValidator", ShimValidator.class);
                    }
                } catch (NoSuchFieldException | IllegalAccessException ex) {
                    LOGGER.error("Installing shim validator failed!", ex);

                    throw new RuntimeException(ex);
                }

                ReportOptions report = new ReportOptions(new LinkedList<Message>() {
                    @Override
                    public boolean add(Message message) {
                        DISPATCHER.dispatchTo(diagnosticListeners, d -> d,
                                Collections.singletonList(new DiagnosticShim(message)));

                        return true;
                    }
                });

                report.setGenerateMetrics(true);
                report.setFilters(Arrays.asList(
                        m -> !m.getID().equals("MISSING_CONFIG"),
                        m -> !m.getID().startsWith("SKIP_")
                ));

                try {
                    LOGGER.trace("Performing validation");

                    validator.addValidatorListener(new ShimListener(validator, eventListeners, metrics));
                    validator.validate(Validator.Format.Tabular, sources, convert(this.config), report);
                    validator.getMetrics()
                            .getEntities()
                            .stream()
                            .map(SourceDetailsShim::new)
                            .forEach(sourceDetails::add);

                    sourceDetails.add(new SourceDetailsShim(ShimValidator.GLOBAL_ENTITY));

                    return true;
                } finally {
                    LOGGER.debug("Validation via shim completed");
                }
            } finally {
                // Reset the classloader
                Thread.currentThread().setContextClassLoader(original);
            }
        }).handle((wasSuccessful, throwable) -> resultBuilder
                .withCompletedSuccessfully(wasSuccessful != null && wasSuccessful)
                .withSources(sourceDetails)
                .withCause(throwable)
                .build()
        );
    }

    private static org.opencdisc.validator.SourceOptions convert(SourceOptions sourceOptions) {
        LOGGER.trace("Converting source options for {}", sourceOptions.getSource());

        org.opencdisc.validator.SourceOptions convertedOptions =
                new org.opencdisc.validator.SourceOptions(sourceOptions.getSource());

        convertedOptions.setName(sourceOptions.getName());
        convertedOptions.setSubname(sourceOptions.getSubname());
        convertedOptions.setType(org.opencdisc.validator.SourceOptions.Type.valueOf(
            ((SourceOptions.StandardTypes)sourceOptions.getType()).name()
        ));

        if (sourceOptions.hasProperty("Delimiter")) {
            convertedOptions.setDelimiter(sourceOptions.getProperty("Delimiter"));
        }

        if (sourceOptions.hasProperty("Qualifier")) {
            convertedOptions.setQualifier(sourceOptions.getProperty("Qualifier"));
        }

        LOGGER.trace("Done converting source options for {}", sourceOptions.getSource());

        return convertedOptions;
    }

    private static org.opencdisc.validator.ConfigOptions convert(ConfigOptions configOptions) {
        LOGGER.trace("Converting configuration options for {}", configOptions.getConfigs());

        org.opencdisc.validator.ConfigOptions convertedOptions =
                new org.opencdisc.validator.ConfigOptions(configOptions.getConfigs().get(0));

        convertedOptions.setDefine(configOptions.getDefine());

        for (String property : configOptions.getProperties()) {
            convertedOptions.setProperty(property, configOptions.getProperty(property));
        }

        LOGGER.trace("Done converting configuration options for {}", configOptions.getConfigs());

        return convertedOptions;
    }

    private class ShimListener implements ValidatorListener {
        private final Validator validator;
        private final Set<Consumer<ValidationEvent>> eventListeners;
        private final DataValidationMetricShim metrics;

        ShimListener(
                Validator validator,
                Set<Consumer<ValidationEvent>> eventListeners,
                DataValidationMetricShim metrics) {
            this.validator = validator;
            this.eventListeners = eventListeners;
            this.metrics = metrics;
        }

        @Override
        public void validatorProgressUpdated(ValidatorEvent validatorEvent) {
            LOGGER.trace("Got validation event type {}", validatorEvent.getState());
            notifyListeners(new ValidationEventImpl(validatorEvent));
            switch (validatorEvent.getState()) {
                case Configured:
                    this.metrics.setInternalMetrics(this.validator.getMetrics().getRuleMetrics());
                    this.dispatchRules();
                    break;
            }
        }

        @Override
        public void validatorStarted(ValidatorEvent validatorEvent) {
            notifyListeners(new ValidationEventImpl(validatorEvent));
        }

        @Override
        public void validatorStopped(ValidatorEvent validatorEvent) {
            notifyListeners(new ValidationEventImpl(validatorEvent));
        }

        private void notifyListeners(ValidationEvent event) {
            for (Consumer<ValidationEvent> listener : eventListeners) {
                listener.accept(event);
            }
        }

        private void dispatchRules() {
            LOGGER.debug("Attempting to dispatch rule creation events");

            try {
                Map<String, MessageTemplate> rules = ShimValidator.getRules()
                        .stream()
                        .peek(r -> DISPATCHER.dispatchTo(ruleListeners, l -> l::acceptTemplate, new RuleInstanceImpl(r)))
                        .collect(Collectors.toMap(MessageTemplate::getID, v -> v));

                LOGGER.trace("Found {} rules", rules.size());

                RuleConverter converter = new RuleConverter(rules, this.metrics);

                ShimValidator.getEntities()
                        .stream()
                        .peek(e -> {
                            String domain = e.getDetails().getString(EntityDetails.Property.Name);
                            MessageTemplate rule = rules.get("SD0062");

                            LOGGER.trace("Peeking at {} to see if we should create SD0062 ({})", domain,
                                    rule != null ? "yes" : "no");

                            if (rule != null) {
                                DISPATCHER.dispatchTo(ruleListeners, l -> l::acceptInstance, new RuleInstanceImpl(
                                    domain,
                                    rule.getID(),
                                    null,
                                    rule.getCategory(),
                                    rule.getMessage(),
                                    rule.getDescription(),
                                    rule.getType()
                                ));
                            }
                        })
                        .filter(e -> e.getConfiguration() != null)
                        .flatMap(e -> {
                            Configuration config = e.getConfiguration();

                            return Stream.concat(
                                config.getRules(EntityDetails.Reference.Metadata).stream(),
                                config.getRules(EntityDetails.Reference.Data).stream()
                            ).map(r -> Pair.of(e.getDetails().getString(EntityDetails.Property.Name), r));
                        })
                        .map(converter::convert)
                        .filter(Objects::nonNull)
                        .forEach(r -> DISPATCHER.dispatchTo(ruleListeners, l -> l::acceptInstance, r));
            } finally {
                DISPATCHER.dispatchTo(ruleListeners, d -> d::complete);
            }
        }
    }

    private static final class ValidationEventImpl implements ValidationEvent {
        private final ValidatorEvent validatorEvent;

        private ValidationEventImpl(ValidatorEvent validatorEvent) {
            this.validatorEvent = validatorEvent;
        }

        @Override
        public long getCurrent() {
            return validatorEvent.getCurrent();
        }

        @Override
        public long getMaximum() {
            return validatorEvent.getMaximum();
        }

        @Override
        public String getName() {
            return validatorEvent.getName();
        }

        @Override
        public State getState() {
            ValidatorEvent.State state = validatorEvent.getState();
            if (state == null) {
                return  State.Start;
            }
            if (state == ValidatorEvent.State.Completed) {
                return State.Stop;
            }
            return State.InProgress;
        }

        @Override
        public ValidationEvent getSubevent() {
            ValidatorEvent subEvent = validatorEvent.getSubevent();
            return subEvent == null ? null : new ValidationEventImpl(subEvent);
        }

        @Override
        public long getTimestamp() {
            return validatorEvent.getTimestamp();
        }

        @Override
        public Type getType() {
            return Type.Processing;
        }
    }

    private static class RuleConverter {
        private static final Field RULE_CONTEXT;
        private static final Field RULE_MESSAGE;
        private static final Field RULE_TYPE;

        static {
            try {
                RULE_CONTEXT = AbstractValidationRule.class.getDeclaredField("context");
                RULE_CONTEXT.setAccessible(true);

                RULE_MESSAGE = AbstractValidationRule.class.getDeclaredField("message");
                RULE_MESSAGE.setAccessible(true);

                RULE_TYPE = AbstractValidationRule.class.getDeclaredField("type");
                RULE_TYPE.setAccessible(true);
            } catch (NoSuchFieldException ex) {
                LOGGER.error("Unable to reflectively get fields needed to shim rule creation", ex);

                throw new RuntimeException(ex);
            }
        }

        private final Map<String, MessageTemplate> rules;
        private final DataValidationMetricShim metrics;

        RuleConverter(Map<String, MessageTemplate> rules, DataValidationMetricShim metrics) {
            this.rules = rules;
            this.metrics = metrics;
        }

        RuleInstance convert(Pair<String, ValidationRule> domainRule) {
            String domain = domainRule.getKey();
            ValidationRule rule = domainRule.getValue();

            LOGGER.trace("Attempting to convert the rule {}:{}", domain, rule.getID());

            if (!(rule instanceof AbstractValidationRule)) {
                LOGGER.error("Rule type {} is not an instance of AbstractValidationRule", rule.getClass());

                throw new IllegalArgumentException("Rule types which don't inherit from AbstractValidationRule " +
                        "are not supported by this shim");
            }

            MessageTemplate template = this.rules.get(rule.getID());

            if (template != null) {
                try {
                    return this.metrics.record(new RuleInstanceImpl(
                        domain,
                        rule.getID(),
                        (String)RULE_CONTEXT.get(rule),
                        template.getCategory(),
                        (String)RULE_MESSAGE.get(rule),
                        template.getDescription(),
                        (Message.Type)RULE_TYPE.get(rule)
                    ));
                } catch (IllegalAccessException ex) {
                    LOGGER.error("Unable to access value when attempting to create rule instance", ex);
                }
            } else {
                LOGGER.error("Shim tried to create rule instance for {} but no such rule exists?", rule.getID());
            }

            return null;
        }
    }
}
