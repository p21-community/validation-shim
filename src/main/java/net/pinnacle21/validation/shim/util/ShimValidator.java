/*
 * Copyright © 2008-2018 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open
 * Source Software License located at [https://www.pinnacle21.com/license]
 * (the "License").
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, and is distributed "AS IS," "WITH ALL FAULTS," and
 * without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.
 * See the License for more details.
 */

package net.pinnacle21.validation.shim.util;

import com.google.common.collect.ImmutableMap;
import org.opencdisc.validator.EventDispatcher;
import org.opencdisc.validator.data.InternalEntityDetails;
import org.opencdisc.validator.engine.BlockValidator;
import org.opencdisc.validator.engine.EngineEntity;
import org.opencdisc.validator.model.EntityDetails;
import org.opencdisc.validator.model.SimpleMetrics;
import org.opencdisc.validator.report.MessageTemplate;
import org.opencdisc.validator.report.Reporter;
import org.opencdisc.validator.settings.Configuration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ShimValidator extends BlockValidator {
    private static final Logger LOGGER = LoggerFactory.getLogger(ShimValidator.class);
    private static final ThreadLocal<ShimValidator> INSTANCE = new ThreadLocal<>();

    public static final InternalEntityDetails GLOBAL_ENTITY = new InternalEntityDetails(EntityDetails.Reference.Data,
            ImmutableMap.of(
                    EntityDetails.Property.Name, "GLOBAL",
                    EntityDetails.Property.Location, "",
                    EntityDetails.Property.Label, "Global Metadata"
            ), null);

    private final List<EngineEntity> engineEntities = new ArrayList<>();
    private final Reporter reporter;

    public ShimValidator(EventDispatcher dispatcher, SimpleMetrics metrics, Reporter reporter, Configuration global) {
        super(dispatcher, metrics, reporter, global);

        LOGGER.debug("Shim validator created on thread {}", Thread.currentThread());

        this.reporter = reporter;
        this.engineEntities.add(new EngineEntity(null, null) {
            @Override
            public Configuration getConfiguration() {
                return global;
            }

            @Override
            public InternalEntityDetails getDetails() {
                return GLOBAL_ENTITY;
            }
        });

        INSTANCE.set(this);
    }

    @Override
    public void prepare(EngineEntity entity) {
        this.engineEntities.add(entity);
        super.prepare(entity);
    }

    @Override
    public void validate() {
        LOGGER.debug("Validation invoked via shim validator");

        super.validate();
        INSTANCE.remove();
    }

    public static List<EngineEntity> getEntities() {
        ShimValidator instance = INSTANCE.get();

        LOGGER.trace("Attempting to get entities from shim validator on thread {}", Thread.currentThread());

        return instance != null
                ? Collections.unmodifiableList(instance.engineEntities)
                : Collections.emptyList();
    }

    public static List<MessageTemplate> getRules() {
        ShimValidator instance = INSTANCE.get();

        LOGGER.trace("Attempting to get rules from shim validator on thread {}", Thread.currentThread());

        return instance != null
                ? instance.reporter.getMessages()
                : Collections.emptyList();
    }
}
