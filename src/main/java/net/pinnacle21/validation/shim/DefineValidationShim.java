/*
 * Copyright © 2008-2018 Pinnacle 21 LLC
 *
 * This file is part of Pinnacle 21 Community.
 *
 * Pinnacle 21 Community is free software licensed under the Pinnacle 21 Open
 * Source Software License located at [https://www.pinnacle21.com/license]
 * (the "License").
 *
 * Pinnacle 21 Community is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY, and is distributed "AS IS," "WITH ALL FAULTS," and
 * without the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.
 * See the License for more details.
 */

package net.pinnacle21.validation.shim;

import com.google.common.collect.ImmutableMap;
import net.pinnacle21.parsing.xml.ElementList;
import net.pinnacle21.parsing.xml.XmlUtils;
import net.pinnacle21.parsing.xml.XmlWriter;
import net.pinnacle21.validation.shim.models.DefineValidationMetricsShim;
import net.pinnacle21.validation.shim.models.DiagnosticShim;
import net.pinnacle21.validation.shim.models.RuleInstanceImpl;
import net.pinnacle21.validation.shim.models.SourceDetailsShim;
import net.pinnacle21.validator.api.BaseValidation;
import net.pinnacle21.validator.api.events.util.Dispatcher;
import net.pinnacle21.validator.api.model.*;
import org.apache.commons.lang3.tuple.Pair;
import org.opencdisc.define.util.Functions;
import org.opencdisc.define.validator.DefineValidator;
import org.opencdisc.define.validator.MessageTransformer;
import org.opencdisc.validator.ReportOptions;
import org.opencdisc.validator.data.InternalEntityDetails;
import org.opencdisc.validator.model.EntityDetails;
import org.opencdisc.validator.model.Message;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import java.io.*;
import java.util.*;
import java.util.concurrent.*;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import static net.pinnacle21.parsing.xml.XmlUtils.every;
import static net.pinnacle21.parsing.xml.XmlUtils.only;

public class DefineValidationShim extends BaseValidation<DefineValidationShim> {
    private static final Logger LOGGER = LoggerFactory.getLogger(DefineValidationShim.class);
    private static final Dispatcher DISPATCHER = new Dispatcher(c -> LoggerFactory.getLogger(c)::error);
    private static final String CONFIG_NAMESPACE_URI = "http://www.opencdisc.org/schema/validator";

    private final SourceOptions source;
    private final ConfigOptions config;
    private final ValidationOptions options;
    private final ExecutorService sharedExecutor;

    DefineValidationShim(SourceOptions source, ConfigOptions config, ValidationOptions options,
            ExecutorService sharedExecutor) {
        this.source = source;
        this.config = config;
        this.options = options != null
                ? options
                : ValidationOptions.builder().build();
        this.sharedExecutor = sharedExecutor;
    }

    @Override
    protected DefineValidationShim recreate() {
        return new DefineValidationShim(this.source, this.config, this.options, this.sharedExecutor);
    }

    @Override
    public CompletionStage<ValidationResult> executeAsync(CancellationToken cancellationToken) {
        DefineValidationMetricsShim metrics = new DefineValidationMetricsShim();
        ValidationResult.Builder resultBuilder = ValidationResult.builder()
                .withMetrics(metrics);
        ExecutorService executor = this.sharedExecutor != null
                ? this.sharedExecutor
                : ForkJoinPool.commonPool();
        SourceDetails sourceDetails = new SourceDetailsShim(new InternalEntityDetails(EntityDetails.Reference.Data,
                ImmutableMap.of(
                    EntityDetails.Property.Name, "DEFINE",
                    EntityDetails.Property.Location, this.source.getSource(),
                    EntityDetails.Property.Label, "Define.xml"
                ), null));

        return CompletableFuture.supplyAsync(() -> {
            ReportOptions reportOptions = new ReportOptions(new LinkedList<>());

            String defineConfig = this.config.getConfigs().get(0);
            String standardConfig = this.config.getConfigs().get(1);
            String[] terminologies;

            try {
                terminologies = determineTerminologies(standardConfig, this.config);

                this.dispatchRules(defineConfig);
            } catch (ParserConfigurationException | SAXException | IOException ex) {
                LOGGER.error("Unexpected exception while trying to read standard configuration file {}",
                        standardConfig, ex);

                return false;
            }

            List<Message> messages = null;

            // dom4j does some really stupid shit to load DocumentFactory as a singleton, so we need to set
            // the context classloader to prevent cast exceptions ¯\_(ツ)_/¯
            ClassLoader original = Thread.currentThread().getContextClassLoader();

            Thread.currentThread().setContextClassLoader(DefineValidationShim.class.getClassLoader());

            try {
                DefineValidator validator = new DefineValidator(this.source.getSource(), reportOptions, defineConfig,
                        terminologies, new RemoteStandardLookup(new File(standardConfig)));

                validator.configure();

                if (validator.validateSchema()) {
                    validator.validateContent();
                }

                MessageTransformer transformer = validator.getMessageTransformer();

                transformer.cleanMessages();

                messages = transformer.getGeneratedMessages();

                return true;
            } finally {
                List<Diagnostic> diagnostics = null;

                if (messages != null) {
                     diagnostics = messages.stream()
                            .map(DiagnosticShim::new)
                            .peek(metrics::recordFailure)
                            .collect(Collectors.toList());

                     diagnostics.stream()
                            // Get distinct issues
                            .filter(distinctBy(d -> Pair.of(d.getId(), d.getContext())))
                            .map(DiagnosticRuleInstance::new)
                            .forEach(d -> DISPATCHER.dispatchTo(this.ruleListeners, l -> l::acceptInstance, d));
                }

                DISPATCHER.dispatchTo(this.ruleListeners, d -> d::complete);

                if (diagnostics != null) {
                    DISPATCHER.dispatchTo(this.diagnosticListeners, d -> d, diagnostics);
                }

                // Reset the classloader
                Thread.currentThread().setContextClassLoader(original);
            }
        }, executor).handle((wasSuccessful, throwable) -> resultBuilder
                .withCompletedSuccessfully(wasSuccessful != null && wasSuccessful)
                .withSources(Collections.singleton(sourceDetails))
                .withCause(throwable)
                .build()
        );
    }

    private void dispatchRules(String defineConfig) throws ParserConfigurationException, SAXException, IOException {
        try (
            InputStream is = new FileInputStream(defineConfig);
            InputStream bs = new BufferedInputStream(is)
        ) {
            Document document = XmlUtils.newSafeDocumentBuilder().parse(bs);
            Element metadata = only("MetaDataVersion", document);

            Element rules = only(CONFIG_NAMESPACE_URI, "ValidationRules", metadata);

            if (rules != null) {
                for (Element rule : new ElementList(rules.getChildNodes())) {
                    DISPATCHER.dispatchTo(this.ruleListeners, l -> l::acceptTemplate, new RuleInstanceImpl(
                        null,
                        rule.getAttribute("ID"),
                        null,
                        rule.getAttribute("Category"),
                        rule.getAttribute("Message"),
                        rule.getAttribute("Description"),
                        Message.Type.fromString(rule.getAttribute("Type"))
                    ));
                }
            }
        }
    }

    private static String[] determineTerminologies(String standardConfig, ConfigOptions configOptions)
            throws ParserConfigurationException, SAXException, IOException {
        try (
            InputStream is = new FileInputStream(standardConfig);
            InputStream bs = new BufferedInputStream(is)
        ) {
            Document document = XmlUtils.newSafeDocumentBuilder().parse(bs);
            Element metadata = only("MetaDataVersion", document);

            List<String> paths = new ArrayList<>();

            for (Element terminologyRef : every(CONFIG_NAMESPACE_URI, "TerminologyRef", metadata)) {
                String path = terminologyRef.getAttributeNS(XmlWriter.XLINK_NAMESPACE_URI, "href");

                if (!path.isEmpty()) {
                    for (String property : configOptions.getProperties()) {
                        path = path.replace("%System." + property + "%", configOptions.getProperty(property));
                    }

                    if ((new File(path)).exists()) {
                        paths.add(path);
                    } else {
                        LOGGER.warn("Unable to resolve terminology path {}, skipping (this might be expected!)", path);
                    }
                }
            }

            return paths.toArray(new String[0]);
        }
    }

    private static <T, U> Predicate<T> distinctBy(Function<T, U> criteria) {
        Set<U> found = ConcurrentHashMap.newKeySet();

        return i -> found.add(criteria.apply(i));
    }

    private static class RemoteStandardLookup implements Functions.F2<File, String, String> {
        private final File config;

        RemoteStandardLookup(File config) {
            this.config = config;
        }

        @Override
        public File apply(String standardName, String standardVersion) {
            return this.config;
        }
    }

    private static class DiagnosticRuleInstance implements RuleInstance {
        private final Diagnostic diagnostic;

        DiagnosticRuleInstance(Diagnostic diagnostic) {
            this.diagnostic = diagnostic;
        }

        @Override
        public String getDomain() {
            return this.diagnostic.getSourceDetails().getString(SourceDetails.Property.Name);
        }

        @Override
        public String getContext() {
            return this.diagnostic.getContext();
        }

        @Override
        public String getCategory() {
            return this.diagnostic.getCategory();
        }

        @Override
        public String getId() {
            return this.diagnostic.getId();
        }

        @Override
        public String getDescription() {
            return this.diagnostic.getDescription();
        }

        @Override
        public String getMessage() {
            return this.diagnostic.getMessage();
        }

        @Override
        public String getPublisherId() {
            return null;
        }

        @Override
        public Diagnostic.Type getType() {
            return this.diagnostic.getType();
        }
    }
}
